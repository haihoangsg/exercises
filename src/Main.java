import java.util.Scanner;

public class Main {
    public static void main(String[] args) {


        // String Exercises 9
        System.out.println("String Exercises 9");

        String str1 = "Java exercises";
        if (str1.endsWith("ses")) {
            System.out.println("The Given String ends with ses");

        }

        // String Exercises 10
        System.out.println("-------------------");
        System.out.println("String Exercises 10");


        String str2 = "Aa kiu, I swd skieo 236587. GH kiu: sieo?? 25.33";
        count(str2);

    }

    public static void count(String x) {
        char[] ch = x.toCharArray();
        int letter = 0;
        int space = 0;
        int numb = 0;
        int other = 0;
        for (int i = 0; i < x.length(); i++) {
            if (Character.isLetter(ch[i])) {
                letter++;
            } else if (Character.isDigit(ch[i])) {
                numb++;
            } else if (Character.isSpaceChar(ch[i])) {
                space++;
            } else {
                other++;
            }
        }
        System.out.println("''The string: Aa kiu, I swd skieo 236587. GH kiu: sieo?? 25.33'' has:");
        System.out.println(letter + " letters");
        System.out.println(space + " spaces");
        System.out.println(numb + " numbers");
        System.out.println(other + " others");


        // Loops Exercises 8
        System.out.println("-------------------");
        System.out.println("Loops Exercises 8");

        int n;
        int status = 1;
        int num = 3;


        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the value of n:");

        n = scanner.nextInt();
        if (n >= 1) {
            System.out.println("First " + n + " prime numbers are:");
            System.out.println(2);
        }

        for (int i = 2; i <= n; ) {
            for (int j = 2; j <= Math.sqrt(num); j++) {
                if (num % j == 0) {
                    status = 0;
                    break;
                }
            }
            if (status != 0) {
                System.out.println(num);
                i++;
            }
            status = 1;
            num++;
        }

        // Loops Exercises 10
        System.out.println("-------------------");
        System.out.println("Loops Exercises 10");

        int i, f = 1;
        int m;
        Scanner s = new Scanner(System.in);
        System.out.print("Enter the number:");
        m = s.nextInt();
        int number = m; //It is the number to calculate factorial
        for (i = 1; i <= number; i++) {
            f = f * i;
        }
        System.out.println("Factorial of " + number + " is: " + f);
    }
}
